﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuGameController : MonoBehaviour {

	public static MenuGameController instance;

	// Outlets
	public Text uiCoins;
	public Text uiHealth;
	public Text uiGameOver;

	public GameObject enemyPrefab;
	
	public MenuGamePlayerController player;

	// Configuration
	
	// State Tracking
	public int coins;

	void Awake() {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		StartCoroutine("SpawnEnemy");
	}
	
	// Update is called once per frame
	void Update () {
		UpdateDisplay();
	}
	
	void UpdateDisplay() {
		uiCoins.text = coins.ToString();
		uiHealth.text = player.health.ToString();
	}
	
	public void GameOver() {
		Time.timeScale = 0;
		uiGameOver.gameObject.SetActive(true);
	}
	
	IEnumerator SpawnEnemy() {
		yield return new WaitForSeconds(0.25f);

		float randomNumber = Random.Range(-5, 5);
		float enemyPosition = -8f;
		if(randomNumber > 0 ) {
			enemyPosition = 8f;
		}
		GameObject newEnemy = Instantiate(enemyPrefab, new Vector3(enemyPosition, 0.5f, 0), Quaternion.identity) as GameObject;
		
		StartCoroutine("SpawnEnemy");
	}
	
	public void ButtonPressedUpgrade() {
		if(coins >= 10) {
			coins -= 10;
			player.attackStrength = 100;
		}
	}
}
