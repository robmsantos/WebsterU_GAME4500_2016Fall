﻿using UnityEngine;
using System.Collections;

public class MenuGamePlayerController : MonoBehaviour {

	// Outlets
	public GameObject projectilePrefab;

	// Configuration

	// State Tracking
	public float health;
	public float attackStrength;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Attack in direction based off of keyboard input
		if(Input.GetKeyDown(KeyCode.LeftArrow)) {
			GameObject newProjectile = Instantiate(projectilePrefab) as GameObject;
			newProjectile.transform.position = transform.position;
			MenuGameProjectileController projectileComponent = newProjectile.GetComponent<MenuGameProjectileController>();
			projectileComponent.direction = -1;
			projectileComponent.damage = attackStrength;
		} else if(Input.GetKeyDown(KeyCode.RightArrow)) {
			GameObject newProjectile = Instantiate(projectilePrefab) as GameObject;
			newProjectile.transform.position = transform.position;
			MenuGameProjectileController projectileComponent = newProjectile.GetComponent<MenuGameProjectileController>();
			projectileComponent.direction = 1;
			projectileComponent.damage = attackStrength;
		}
	}
	
	public void TakeDamage(float amount) {
		health -= amount;
		if(health <= 0) {
			health = 0;
			Die();
		}
	}
	
	void Die() {
		MenuGameController.instance.GameOver();
		Destroy(gameObject);
	}
}
