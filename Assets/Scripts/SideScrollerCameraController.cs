﻿using UnityEngine;
using System.Collections;

public class SideScrollerCameraController : MonoBehaviour {

	// Outlets
	public GameObject cameraTarget;	// What is the camera following?
	public float followThreshold; // How far before the camera starts to follow?

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		// Target distance
		float distanceFromTarget = Mathf.Abs(transform.position.x - cameraTarget.transform.position.x);

		// If the target is far enough...
		if(distanceFromTarget >= followThreshold) {
			// Follow target (smoothly; don't just jump all the way to it)
			Vector3 newPosition = Vector3.Lerp(transform.position, new Vector3(cameraTarget.transform.position.x, transform.position.y, transform.position.z), 0.05f);
			
			// Set new position
			transform.position = newPosition;
		}

	}
}
